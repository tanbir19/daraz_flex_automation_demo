import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;

public class Automation {
    public static void main(String[] args) throws MalformedURLException {
        DesiredCapabilities dc = new DesiredCapabilities();

        dc.setCapability(MobileCapabilityType.UDID, "R5CR20W5JLD");
        dc.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.daraz.dflex.uat");
        dc.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.daraz.dflex.presentation.splash.SplashActivity");
        System.out.println("initializing connection to Appium Server");
        AndroidDriver<AndroidElement> driver = new AndroidDriver<AndroidElement>(new URL("http://localhost:4723/wd/hub"), dc);
        System.out.println("Connected to Appium Server");

        driver.findElement(By.xpath("//*[@text='Bangladesh']")).click();
        System.out.println("Country Selected");
        driver.findElement(By.xpath("//*[@text='English']")).click();
        System.out.println("Language Selected");
        driver.findElement(By.xpath("//*[@text='Confirm']")).click();
        System.out.println("Confirm Button Tapped");
        driver.findElement(By.xpath("//*[@text='Next']")).click();
        driver.findElement(By.xpath("//*[@text='Next']")).click();
        driver.findElement(By.xpath("//*[@text='Next']")).click();
        System.out.println("All Banner viewed");
        new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='1']")));
        driver.findElement(By.xpath("//*[@text='1']")).click();
        driver.findElement(By.xpath("//*[@text='8']")).click();
        driver.findElement(By.xpath("//*[@text='9']")).click();
        driver.findElement(By.xpath("//*[@text='4']")).click();
        driver.findElement(By.xpath("//*[@text='9']")).click();
        driver.findElement(By.xpath("//*[@text='2']")).click();
        driver.findElement(By.xpath("//*[@text='8']")).click();
        driver.findElement(By.xpath("//*[@text='6']")).click();
        driver.findElement(By.xpath("//*[@text='4']")).click();
        driver.findElement(By.xpath("//*[@text='5']")).click();
        driver.findElement(By.xpath("//*[@text='Done']")).click();
        System.out.println("number 1894928645 given input box");
        new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@text='Continue']")));
        driver.findElement(By.xpath("//*[@text='Continue']")).click();
        System.out.println("OTP sent");
//        WebDriverWait waitForToast = new WebDriverWait(driver,25);
//
//        waitForToast.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//android.widget.Toast")));
//        try {
//            Thread.sleep(3000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        System.out.println("Calling toast");
        String toastMessage = driver.findElement (By.xpath("//android.widget.Toast")).getText();
        System.out.println("called toast");

        String otp = toastMessage.substring(toastMessage.length()-4);
        char firstChar = otp.charAt(0);
        char secondChar = otp.charAt(1);
        char thirdChar = otp.charAt(2);
        char forthChar = otp.charAt(3);

        new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@class='android.widget.EditText']")));
        driver.findElement(By.xpath("//*[@class='android.widget.EditText']")).click();
        driver.findElement(By.xpath("//*[@text='" + firstChar + "']")).click();
        driver.findElement(By.xpath("//*[@text='" + secondChar + "']")).click();
        driver.findElement(By.xpath("//*[@text='" + thirdChar + "']")).click();
        driver.findElement(By.xpath("//*[@text='" + forthChar + "']")).click();
        driver.findElement(By.xpath("//*[@text='Done']")).click();
        driver.findElement(By.xpath("//*[@text='Verify']")).click();


    }



}
